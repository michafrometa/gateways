# Gateways
Hi, this is a simple project to improve my skills with Spring Boot.
It contains my own solution for [a test](TEST.md) requested to get a job in [a software company](https://www.musala.com/).

Basically it contains: 
1: Spring MVC

2: Spring DATA JPA.

3: H2 Database
